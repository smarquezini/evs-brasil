#
# Create a new page for each car
#
#

require 'json'

module Jekyll

  class CarPage < Jekyll::Page
    def initialize(site, base, id, car)
    	@site = site
    	@base = base
    	@dir = "cars"
    	@name = id + ".html"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "car.html")
    	self.data["title"] = id
    	self.data["layout"] = "car"
	self.data["car"] = car
	self.data["car-id"] = id
    end
  end

  class CarGenerator < Jekyll::Generator

    def generate(site)
    	cars = site.data["cars"]
    	path = "_site/data"
    	FileUtils.mkpath(path) unless File.exist?(path)
	carsIds = []
	
	cars.each do |id,car|

		if not car["battery-estimate-kwh"]
			v = site.data["vehicles"][car["vehicle"]]
			if v["battery-kwh"]
				car["battery-estimate-kwh"] = v["battery-kwh"]
			end
		end

		car["kms"].each do |kmData|
			if kmData["battery-estimate-kwh"] and not kmData["battery-estimate"]
				if car["battery-estimate-kwh"]					
						kmData["battery-estimate"] = ((kmData["battery-estimate-kwh"].to_f / car["battery-estimate-kwh"]) * 100).round(0)
				end
			end
		end
		site.pages.push(CarPage.new(site, site.source, id, car))
		site.pages.push(JsonDataPage.new(site, site.source, "cars", id, car ))

		carsIds.push(id)
		
	end	    	
	site.pages.push(JsonDataPage.new(site, site.source, "cars", "cars-ids", carsIds))
	site.pages.push(JsonDataPage.new(site, site.source, "cars", "vehicles", cars))
    end

  end
  
end

